<?php
	require_once("util.php");
	if(empty($_SESSION['intentos'])){
        $_SESSION['intentos'] = 0;
    }
	if($_SESSION['intentos'] > 3) {
        die('Demasiados intentos de acceso. Bloqueado por seguridad.');
    }

	// Loguin de administrador
	if(isset($_POST['acceder'])){
		$_SESSION['admin'] = ($_POST['pass']=="Laguarres15");
        $_SESSION['intentos']++;
	}

	// Subir imagen cabecera
	if(isset($_POST['subirCabecera']) && $_SESSION['admin']){
		if(!is_dir("images")){ mkdir("images"); }
		$type = explode('image/',$_FILES['imagen']['type'])[1];
		$img = time().'.'.$type;
        @chmod('images',0777);
		if(copy($_FILES['imagen']['tmp_name'],"images/$img") && subirImagenCabecera($img)){
			?><script type="text/javascript">alert('Imagen subida correctamente');</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para subir la imagen');</script><?php
		}
	}

	// Eliminar imagen cabecera
	if(isset($_POST['eliminarCabecera']) && $_SESSION['admin']){
		if(eliminarImagenCabecera($_POST['imagenVieja'])){
			unlink('images/'.$_POST['imagenVieja']);
			?><script type="text/javascript">alert('Imagen eliminada correctamente');</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para eliminar la imagen');</script><?php
		}
	}

	// Subir nosotros
	if(isset($_POST['subirNosotros']) && $_SESSION['admin']){
		$type = explode('image/',$_FILES['imagen']['type'])[1];
		$img = time().'.'.$type;
		$nombre = trim($_POST['nombre']);
		$curriculum = trim($_POST['curriculum']);
        @chmod('images',0777);
		if(copy($_FILES['imagen']['tmp_name'],"images/$img") && subirNosotros($nombre,$curriculum,$img)){
			?><script type="text/javascript">alert('Nueva persona subida a NOSOTROS correctamente');</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para subir el currículum de la persona');</script><?php
		}
	}

	// Eliminar nosotros
	if(isset($_POST['eliminarNosotros']) && $_SESSION['admin']){
		if(eliminarNosotros($_POST['eliminar'])){
			?><script type="text/javascript">alert('Persona eliminada correctamente de la sección NOSOTROS');</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para eliminar la persona');</script><?php
		}
	}

	// Subir curso
	if(isset($_POST['subirCurso']) && $_SESSION['admin']){
		if(!is_dir("images")){ mkdir("images"); }
		$type = explode('image/',$_FILES['imagen']['type'])[1];
		$img = time().'.'.$type;
		$titulo = trim($_POST['titulo']);
		$texto = trim($_POST['texto']);
		@chmod('images',0777);
		if(copy($_FILES['imagen']['tmp_name'],"images/$img") && subirCurso($titulo,$texto,$img)){
			?><script type="text/javascript">alert('Curso subido correctamente');</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para subir el curso');</script><?php
		}
	}

	// Editar curso
	if(isset($_POST['editarCurso']) && $_SESSION['admin']){
		header("Location:adminCurso.php?curso=".$_POST['curso']);
	}

	// Editar datos de contacto
	if(isset($_POST['contacto']) && $_SESSION['admin']){
		$direccion = trim($_POST['direccion']);
		$email = trim($_POST['email']);
		$telefono1 = trim($_POST['telefono1']);
		$telefono2 = trim($_POST['telefono2']);
		actualizaContacto($direccion, $email, $telefono1, $telefono2);
	}

	// Eliminar curso
	if(isset($_POST['eliminarCurso']) && $_SESSION['admin']){
		if(eliminarCurso($_POST['curso'])){
			?><script type="text/javascript">alert('Curso eliminado correctamente');</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para eliminar el curso');</script><?php
		}
	}

	// Subir evento
	if(isset($_POST['subirEvento']) && $_SESSION['admin']){
		if(!is_dir("images")){ mkdir("images"); }
		$type = explode('image/',$_FILES['imagen']['type'])[1];
		$img = time().'.'.$type;
		$titulo = trim($_POST['titulo']);
		$texto = trim($_POST['texto']);
		if(copy($_FILES['imagen']['tmp_name'],"images/$img")
			&& subirEvento($titulo,$texto,$img)){
			?><script type="text/javascript">alert('Evento subido correctamente');</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para subir el evento');</script><?php
		}
	}

	// Eliminar evento
	if(isset($_POST['eliminarEvento']) && $_SESSION['admin']){
		if(eliminarEvento($_POST['evento'])){
			?><script type="text/javascript">alert('Evento eliminado correctamente');</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para eliminar el evento');</script><?php
		}
	}

 	$contacto = cargaContacto();
?>
<!DOCTYPE html>
<html lang="es" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
	<head>
		<title>Global MakeUP</title>
		<meta name="author" content="Fran Roldan" />
		<meta name="robots" content="index, follow" />
		<meta name="language" content="es" />
		<meta name="keywords" content="zaragoza,maquillaje,escuela,cursos,global makeup,bodas,modulos,profesion" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Haz de tu pasión tu profesión">
		<link rel="shortcut icon" href="assets/images/favicon.png" class="favicon_icon">
		<link rel="stylesheet" href="css/superfish.css" media="screen">
		<link href="css/style_font.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/rs-plugin/css/responsive.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/rs-plugin/css/settings.css" media="screen" />
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
		<link href="css/style_default.css" rel="stylesheet" >
		<link href="css/color_pink.css" rel="stylesheet" id="clrswitch">
		<link href="css/color_bg_white.css" rel="stylesheet" id="clrswitch_bg">
		<link href="css/style-switcher.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="assets/plugin/style/css/style_iso.css" media="all" />
		<link rel="stylesheet" type="text/css" href="assets/plugin/style/css/media-queries.css" media="all" />
		<link href="css/uikit.css" rel="stylesheet">
		<link href="css/bjqs.css" rel="stylesheet">
		<link href="css/demo.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			input{ display:inline !important; }
			option{
				width:180px;
				height:100px;
				border:solid 1px black;
				background-repeat:no-repeat;
				background-size:contain;
				background-position:center;
			}
		</style>
	</head>
	<body>
		<nav class="light">
			<div class="navbar navbar-default navbar-static-top">
				<div class="container clearfix">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="http://www.globalmakeup.es" class="botonArriba">
							<p class="logo_main">
								<img src="assets/images/slide/logo.png" style="max-width:100%;margin-top:-10px;"/>
							</p>
						</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="sf-menu" id="nav" style="margin-left:11.5%;"></ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="section-more-feathurs" style="padding:0 !important;clear:both;" id="index">
			<div class="container feathurs-cointain">
				<div class="row">
					<h3 style="text-align:center;color:#f637c3;">Administración de la web</h3>
					<p style="padding:0px;margin:15px;"><br/><hr style="padding:0;margin:0;"></p>
					<div class="col-sm-12 feathurs-part">
						<?php if(!isset($_SESSION['admin']) || $_SESSION['admin']!=true){ ?>
						<form action="admin.php" method="POST">
							<p class="disc-text">
								Contrase&ntilde;a: <input type="password" size="10" name="pass" />
								<input type="submit" name="acceder" value="Acceder" />
							</p>
						</form>
						<?php }else{ ?>
						<p class="disc-text" style="margin-bottom:15px;">
							Las imágenes para la <b>CABECERA</b> deben tener un tamaño exacto de <b>1800 x 1012</b> píxels (ancho x alto).
							Un tamaño de imágen diferente puede hacer que el diseño pierda su estructura óptima.
						</p>
						<form action="admin.php" method="POST" enctype="multipart/form-data">
							<input type="file" name="imagen" /> &nbsp; &nbsp;
							<input type="submit" name="subirCabecera" value="Nueva imagen para la cabecera" />
							<br/><br/>
							<select name="imagenVieja">
								<?php foreach(cargaImagenesCabecera() as $imagen){ ?>
									<option style="background-image:url(images/<?=$imagen?>);" value="<?=$imagen?>"><?=$imagen?></option>
								<?php } ?>
							</select>
							<input type="submit" name="eliminarCabecera" value="Eliminar foto de cabecera" />
						</form>
						<p style="padding:0px;margin:15px;"><hr style="padding:0;margin:0;"></p>
						<p class="disc-text" style="margin-bottom:15px;">
							Las imágenes para los <b>CURSOS</b> deben tener un tamaño exacto de <b>270 x 270 píxels</b> (ancho x alto).
							Un tamaño de imágen diferente puede hacer que el diseño pierda su estructura óptima.
						</p>
						<form action="admin.php" method="POST" enctype="multipart/form-data">
							Título: <input type="text" name="titulo" required /><br/><br/>
							Imágen: <input type="file" name="imagen" required /><br/><br/>
							Texto: <textarea name="texto" required></textarea><br/><br/>
							<input type="submit" name="subirCurso" value="Nuevo curso" />
						</form>
						<br/><br/>
						<form action="admin.php" method="POST" enctype="multipart/form-data">
							<select name="curso">
								<?php foreach(cargaCursos() as $curso){ ?>
									<option value="<?=$curso->id?>"><?=$curso->titulo?></option>
								<?php } ?>
							</select>
							<input type="submit" name="editarCurso" value="Editar curso" />
							<input type="submit" name="eliminarCurso" value="Eliminar curso" />
						</form>
						<p style="padding:0px;margin:15px;"><hr style="padding:0;margin:0;"></p>
						<p class="disc-text" style="margin-bottom:15px;">
							Las imágenes para los <b>EVENTOS</b> no han de tener un tamaño concreto, aunque <b>se recomienda
							que sean cuadradas</b>. Para no sobrecargar la web se recomiendan tamaños entre 300x300 y 900x900 píxeles.
						</p>
						<form action="admin.php" method="POST" enctype="multipart/form-data">
							Título: <input type="text" name="titulo" required /><br/><br/>
							Imágen: <input type="file" name="imagen" required /><br/><br/>
							Texto: <textarea name="texto" required></textarea><br/><br/>
							<input type="submit" name="subirEvento" value="Nuevo evento" />
						</form>
						<br/><br/>
						<form action="admin.php" method="POST" enctype="multipart/form-data">
							<select name="evento">
								<?php foreach(cargaEventos() as $evento){ ?>
									<option value="<?=$evento->id?>"><?=$evento->titulo?></option>
								<?php } ?>
							</select>
							<input type="submit" name="eliminarEvento" value="Eliminar evento" />
						</form>
						<p style="padding:0px;margin:15px;"><hr style="padding:0;margin:0;"></p>
						<p class="disc-text" style="margin-bottom:15px;">
							Las imágenes para la sección <b>NOSOTROS</b> deben tener un tamaño exacto de <b>305 x 230</b> píxels (ancho x alto).
							Un tamaño de imágen diferente puede hacer que el diseño pierda su estructura óptima.
						</p>
						<form action="admin.php" method="POST" enctype="multipart/form-data">
							Nombre: <input type="text" name="nombre" required /><br/><br/>
							Foto: <input type="file" name="imagen" required /><br/><br/>
							Currículum: <textarea name="curriculum" required></textarea><br/><br/>
							<input type="submit" name="subirNosotros" value="Nueva persona para la sección NOSOTROS" />
						</form>
						<br/><br/>
						<form action="admin.php" method="POST" enctype="multipart/form-data">
							<select name="eliminar">
								<?php foreach(cargaNosotros() as $persona){ ?>
									<option value="<?=$persona->id?>"><?=$persona->nombre?></option>
								<?php } ?>
							</select>
							<input type="submit" name="eliminarNosotros" value="Eliminar persona de la sección NOSOTROS" />
						</form>
						<br/><br/>
						<p style="padding:0px;margin:15px;"><hr style="padding:0;margin:0;"></p>
						<p class="disc-text" style="margin-bottom:15px;">
							Con este formulario puede editar los datos de <b>CONTACTO</b>.
						</p>
						<form action="admin.php" method="POST">
							Email: <input type="text" name="email" value="<?=$contacto->email?>" required /><br/><br/>
							Dirección: <input type="text" name="direccion" value="<?=$contacto->direccion?>" required /><br/><br/>
							Teléfono 1: <input type="text" name="telefono1" value="<?=$contacto->telefono_1?>" required /><br/><br/>
							Teléfono 2: <input type="text" name="telefono2" value="<?=$contacto->telefono_2?>" required /><br/><br/>
							<input type="submit" name="contacto" value="Actualizar datos de contacto" />
						</form>
						<br/><br/><br/>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="section-footer-copyright " >
			<div class="container">
				<div class="col-sm-12 copyright-part">
					<div class="footer_logo">
						<a href="#" class="botonArriba"><img src="assets/images/slide/logo.png" style="width:170px;"/></a>
					</div>
					<div class="clear"></div>
					<div class="copyright-text">
						<span class="color-gray">&copy; Global MakeUp 2015 - Developed by </span>
						<a href="http://www.franroldan.com"><span class="color-red">Fran Roldán</span></a>
					</div>
				</div>
			</div>
		</div>
		<script src="js/jquery.js"></script>
		<script type="text/javascript">
			$(".section-more-feathurs").hide();
			$("document").ready(function() {
                $(".section-more-feathurs").show(3000).delay(800);
				jQuery(document).ready(function($) {
					$('#banner-fade').bjqs({
						'responsive':true,
						'animtype':'slide'
					});
				});
			});
		</script>
		<script src="js/swtch/jquery.cookie.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-modal.js"></script>
		<script src="js/bootstrap-modalmanager.js"></script>
		<script type="text/javascript" src="js/jquery.themepunch.plugins.min.js"></script>
		<script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="assets/plugin/style/js/jquery.isotope.min.js"></script>
		<script src="js/jquery.inview.js"></script>
		<script type="text/javascript" src="js/jquery.easy-pie-chart.js"></script>
		<script type="text/javascript" src="js/jquery.easy-pie-chart1.js"></script>
		<script src="js/jquery.js"></script>
		<script src="js/swtch/jquery.cookie.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-modal.js"></script>
		<script src="js/jquery.smooth-scroll.js"></script>
		<script src="js/parallax.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/uikit.js"></script>
		<script type="text/javascript" src="js/jquery.fitvids.js"></script>
		<script type="text/javascript" src="js/jquery.fittext.js"></script>
		<script src="js/jquery.sticky.js"></script>
		<script src="js/hoverIntent.js"></script>
		<script src="js/superfish.js"></script>
		<script src="js/jquery.nav.js"></script>
		<script src="js/bjqs-1.3.min.js"></script>
		<script src="js/jquery.secret-source.min.js"></script>
		<script type="text/javascript" src="js/reversal-custom_default.js"></script>
	</body>
</html>
