<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mysql = null;

function conexion()
{
    global $mysql;
    if(is_null($mysql)) {
        $bd_location = '127.0.0.1';
        $bd_user = 'root';
        $bd_pass = '';
        $bd_name = 'globalmakeup';
        $bd_port = 3306;
        if (strpos($_SERVER['HTTP_HOST'], 'globalmakeup') !== false) {
            $bd_pass = '859d29498eed0b4a8de06c3a0e8aa96c55cfb5badfc13ff5';
        }
        $mysql = new mysqli($bd_location, $bd_user, $bd_pass, $bd_name, $bd_port);
        if ($mysql->connect_errno) {
            die('Fallo al conectar con MySQL: (' . $mysql->connect_errno . ') ' . $mysql->connect_error);
        }
    }
    return $mysql;
}

function cargaImagenesCabecera()
{
    if(empty($_SESSION['imagenes-cabecera'])){
        $mysql = conexion();
        $imagenes = array();
        $query = $mysql->query("SELECT imagen FROM cabeceras ORDER BY RAND()");
        while ($registro = $query->fetch_object()) {
            array_push($imagenes, $registro->imagen);
        }
        $_SESSION['imagenes-cabecera'] = $imagenes;
    }else{
        $imagenes = $_SESSION['imagenes-cabecera'];
    }
    return $imagenes;
}

function subirImagenCabecera($_imagen)
{
    $mysql = conexion();
    $sql = "INSERT INTO cabeceras (imagen) VALUES ('$_imagen')";
    if(!empty($_SESSION['imagenes-cabecera'])){
        unset($_SESSION['imagenes-cabecera']);
    }
    return (boolean)($query = $mysql->query($sql));
}

function eliminarImagenCabecera($_imagen)
{
    $mysql = conexion();
    $sql = "DELETE FROM cabeceras WHERE imagen LIKE '$_imagen'";
    if(!empty($_SESSION['imagenes-cabecera'])){
        unset($_SESSION['imagenes-cabecera']);
    }
    return (boolean)($query = $mysql->query($sql));
}

function cargaCursos()
{
    if(empty($_SESSION['cursos'])){
        $mysql = conexion();
        $query = $mysql->query("SELECT * FROM cursos ORDER BY RAND()");
        $cursos = array();
        while ($registro = $query->fetch_object()) {
            array_push($cursos, $registro);
        }
        $_SESSION['cursos'] = $cursos;
    }else{
        $cursos = $_SESSION['cursos'];
    }
    return $cursos;
}

function cargaCurso($_id)
{
    $mysql = conexion();
    $query = $mysql->query("SELECT * FROM cursos WHERE id = ".$_id);
    if ($curso = $query->fetch_object()) {
        return $curso;
    }
    return null;
}

function editarcurso($_id, $_texto)
{
    $mysql = conexion();
    $sql = "UPDATE cursos SET texto = '$_texto' WHERE id = ".$_id;
    if(!empty($_SESSION['cursos'])){
        unset($_SESSION['cursos']);
    }
    return (boolean)($query = $mysql->query($sql));
}

function subirCurso($_titulo, $_texto, $_imagen)
{
    $mysql = conexion();
    $sql = "INSERT INTO cursos (titulo,texto,imagen) VALUES ('$_titulo','$_texto','$_imagen')";
    if(!empty($_SESSION['cursos'])){
        unset($_SESSION['cursos']);
    }
    return (boolean)($query = $mysql->query($sql));
}

function eliminarCurso($_id)
{
    $mysql = conexion();
    $sql = "DELETE FROM cursos WHERE id = ".$_id;
    if(!empty($_SESSION['cursos'])){
        unset($_SESSION['cursos']);
    }
    return (boolean)($query = $mysql->query($sql));
}

function cargaEventos()
{
    if(empty($_SESSION['eventos'])) {
        $mysql = conexion();
        $query = $mysql->query("SELECT * FROM eventos ORDER BY RAND()");
        $eventos = array();
        while ($registro = $query->fetch_object()) {
            array_push($eventos, $registro);
        }
        $_SESSION['eventos'] = $eventos;
    }else{
        $eventos = $_SESSION['eventos'];
    }
    return $eventos;
}

function subirEvento($_titulo, $_texto, $_imagen)
{
    $mysql = conexion();
    $sql = "INSERT INTO eventos (titulo,texto,imagen) VALUES ('$_titulo','$_texto','$_imagen')";
    if(!empty($_SESSION['eventos'])){
        unset($_SESSION['eventos']);
    }
    return (boolean)($query = $mysql->query($sql));
}

function eliminarEvento($_id)
{
    $mysql = conexion();
    $sql = "DELETE FROM eventos WHERE id = ".$_id;
    if(!empty($_SESSION['eventos'])){
        unset($_SESSION['eventos']);
    }
    return (boolean)($query = $mysql->query($sql));
}

function cargaNosotros()
{
    if(empty($_SESSION['nosotros'])) {
        $mysql = conexion();
        $query = $mysql->query("SELECT * FROM nosotros ORDER BY RAND()");
        $nosotros = array();
        while ($registro = $query->fetch_object()) {
            array_push($nosotros, $registro);
        }
        $_SESSION['nosotros'] = $nosotros;
    }else{
        $nosotros = $_SESSION['nosotros'];
    }
    return $nosotros;
}

function subirNosotros($_nombre, $_curriculum, $_foto)
{
    $mysql = conexion();
    $sql = "INSERT INTO nosotros (nombre,curriculum,foto) VALUES ('$_nombre','$_curriculum','$_foto')";
    if(!empty($_SESSION['nosotros'])){
        unset($_SESSION['nosotros']);
    }
    return (boolean)($query = $mysql->query($sql));
}


function eliminarNosotros($_id)
{
    $mysql = conexion();
    $sql = "DELETE FROM nosotros WHERE id = ".$_id;
    if(!empty($_SESSION['nosotros'])){
        unset($_SESSION['nosotros']);
    }
    return (boolean)($query = $mysql->query($sql));
}

function cargaContacto()
{
    if(empty($_SESSION['contacto'])){
        $mysql = conexion();
        $query = $mysql->query("SELECT * FROM contacto");
        if ($contacto = $query->fetch_object()) {
            $_SESSION['contacto'] = $contacto;
        }else{
            $_SESSION['contacto'] = null;
        }
    }
    return $_SESSION['contacto'];
}

function actualizaContacto($direccion, $email, $telefono1, $telefono2)
{
    $mysql = conexion();
    $query = "
        UPDATE contacto SET direccion = '{$direccion}',
        email = '{$email}', telefono_1 = '{$telefono1}',
        telefono_2 = '$telefono2' WHERE 1 = 1
    ";
    unset($_SESSION['contacto']);
    $mysql->query($query);
}

function envioEmail($_datos)
{
    require_once('PHPMailer/src/Exception.php');
    require_once('PHPMailer/src/PHPMailer.php');
    require_once('PHPMailer/src/SMTP.php');
    $body = "<h3>Ha recibido un mensaje desde la web:</h3><hr>";
    $body .= "<p>Nombre: <b>{$_datos['nombre']}</b></p>";
    $body .= "<p>Teléfono: <b>{$_datos['telefono']}</b></p>";
    $body .= "<p>Email: <b>{$_datos['email']}</b></p>";
    $body .= "<p>Asunto del mensaje: <b>{$_datos['asunto']}</b></p>";
    $body .= "<hr><p>Mensaje: {$_datos['mensaje']}</p>";
    $mail = new PHPMailer(true);
    try {
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->Username = 'globalmakeup2.zgz@gmail.com';
        $mail->Password = 'Muchu1965';
        $mail->Subject = 'GlobalMakeUP - Mensaje desde la web';
        $mail->addAddress('globalmakeup.zgz@gmail.com');
        $mail->setFrom('globalmakeup.zgz@gmail.com', 'Web GlobalMakeUP');
        $mail->isHTML(true);
        $mail->Body = $body;
        if (!$mail->send()) {
            echo '<pre>Error en el envío del email: <hr>';
            print_r($mail->ErrorInfo);
            echo '</pre>';
            die();
            return false;
        }
    } catch (Exception $e) {
        echo '<pre>Error en el envío del email: <hr>';
        print_r($e);
        print_r($mail->ErrorInfo);
        echo '</pre>';
        die();
        return false;
    }
    return true;
}
