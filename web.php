<?php
    require_once('util.php');
    $imagenes_cabecera = cargaImagenesCabecera();
    $raretime = time()/2*3/4/2;
    $seccode = md5(sha1($raretime));
?>
<!DOCTYPE html>
<html lang="es" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
	<head>
		<title>Global MakeUP</title>
		<meta name="author" content="Fran Roldan" />
		<meta name="robots" content="index, follow" />
		<meta name="language" content="es" />
		<meta name="keywords" content="zaragoza,maquillaje,escuela,cursos,global makeup,bodas,modulos,profesion" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Haz de tu pasión tu profesión">
		<link rel="shortcut icon" href="assets/images/favicon.png" class="favicon_icon">
		<link rel="stylesheet" href="css/superfish.css" media="screen">
		<link href="css/style_font.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/rs-plugin/css/responsive.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/rs-plugin/css/settings.css" media="screen" />
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
		<link href="css/style_default.css" rel="stylesheet" >
		<link href="css/color_pink.css" rel="stylesheet" id="clrswitch">
		<link href="css/style-switcher.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="assets/plugin/style/css/style_iso.css" media="all" />
		<link rel="stylesheet" type="text/css" href="assets/plugin/style/css/media-queries.css" media="all" />
		<link href="css/uikit.css" rel="stylesheet">
		<link href="css/bjqs.css" rel="stylesheet">
		<link href="css/demo.css" rel="stylesheet">
	</head>
	<body>
		<nav class="light">
			<div class="navbar navbar-default navbar-static-top">
				<div class="container clearfix">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#" class="botonArriba">
							<p class="logo_main">
								<img src="assets/images/slide/logo.png" style="max-width:100%;margin-top:-10px;"/>
							</p>
						</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="sf-menu" id="nav" style="margin-left:11.5%;">
							<li><a id="botonInicio">Inicio</a></li>
							<li><a id="botonNosotros" href="#team">Nosotros</a></li>
							<li><a id="botonExperiencias" href="#testimonial">Eventos</a></li>
							<li><a id="botonCursos" href="#portfolio">Cursos</a></li>
							<li><a id="botonContacto" href="#contact">Contacto</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div id="container" style="margin:0; padding:0;">
			<img id="carruselo" src="images/<?=$imagenes_cabecera[0]?>" style="width:100%;height:70%;margin-top:-15px;"
                 title="Imagen cabecera" alt="Imagen cabecera" />
		</div>

		<!-- INICIO -->
		<div style="padding-bottom:0; padding-top:15px; border-top:solid 5px #f637c3; border-bottom:solid 5px #f637c3; clear:both; display:none; z-index:-20;"
            class="section-more-feathurs primerInicio" id="index">
			<div class="container feathurs-cointain">
				<div class="row">
					<h2 style="text-align:center;">¡Bienvenidos a <b>Global Makeup</b>, tu escuela de maquillaje profesional!</h2><br/><br/>
					<div class="col-sm-3 feathurs-part">
						<p class="disc-text" style="margin-bottom:0;z-index:-100;">
							<b>GLOBAL MAKEUP</b> es una escuela pensada para ti, tanto si eres profesional y
							quieres profundizar en el mundo del maquillaje como si quieres iniciarte en
							este mundo maravilloso de la moda y la imagen personal.
						</p>
					</div>
					<div class="col-sm-3 feathurs-part">
						<p class="disc-text" style="margin-bottom:0;z-index:-100;">
							Nuestros cursos están diseñados por formadores del mundo del
							maquillaje, la cosmética, la peluquería y las artes plásticas y
							con ellos aprenderás todo lo que necesitas para desarrollar tu creatividad
							y dedicarte de lleno a esta profesión.
						</p>
					</div>
					<div class="clear" style="display:none"></div>
					<div class="col-sm-3 feathurs-part">
						<p class="disc-text" style="margin-bottom:0;z-index:-100;">
							Tenemos muy en cuenta tus horarios y es por ello que te ofrecemos cursos
							reducidos y amplios horarios incluso durante el fin de semana en un espacio
							agradable y profesional.
						</p>
					</div>
					<div class="col-sm-3 feathurs-part">
						<p class="disc-text" style="margin-bottom:0;z-index:-100;">
							Nuestra Escuela está situada en <b>Vía Hispanidad nº 83 - Zaragoza</b>. Puedes contactar
							con nosotros en los teléfonos <b>619 15 80 63 - 609 93 50 42</b>
							o en el correo electrónico <b>globalmakeup.zgz@gmail.com</b>
						</p>
					</div>
					<div style="clear:both;text-align:center;z-index:-100;">
						<br/><h5>¡Llámanos y concierta una cita, te enseñaremos la escuela y te asesoraremos
						sobre el curso que más se adapta a lo que estás buscando!</h5><br/><br/>
					</div>
				</div>
			</div>
		</div>

		<!-- SEGUNDO INICIO -->
		<div class="section-second-header parallax data-uk-smooth-scroll">
			<div class="container ">
				<div class="row ">
					<div class="col-sm-12 second-header segundoInicio" style="display:none;">
						<div class="second-logo"><img src="assets/images/slide/logo.png" style="width:250px;"/></div>
						<h1 style="font-size:55px;">GLOBAL MAKEUP</h1>
						<p class="disc-text1" style="font-size:27px;">Haz de tu pasión tu profesión</p>
					</div>
				</div>
			</div>
		</div>

		<!-- NOSOTROS -->
		<div id="team" class="section-our-team" style="padding-top:20px !important;padding-bottom:40px !important;">
			<div class="container" style="width:100%;">
				<div class="row" style="width:100%;margin:0 auto;">
					<div style="margin:0 auto;padding:20px;">
						<div class="mark"></div>
						<div class="header-title">
							<a href="#team" class="trade-mark"><h1><br/>NOSOTROS</h1></a>
							<p class="text5">
								Somos un equipo de <b>profesionales del mundo del maquillaje, la cosmética,
								peluquería y las artes plásticas</b> con una dilatada experiencia en cada área
								de trabajo y con la capacidad de conducir a nuestros estudiantes por
								satisfactorios procesos de aprendizaje, tanto teórico como práctico.
							</p>
						</div>
					</div>
				</div>
				<?php $i=0; foreach (cargaNosotros() as $persona) {
				    if ($i%3==0) {
                        ?><div class="row" style="width:100%;margin:0 auto !important;"><?php
                    } ?>
					<div class="col-sm-4 our-team" style="border:0;margin:0 !important;padding-bottom:20px;">
						<img src="images/<?=$persona->foto?>" alt="<?=$persona->nombre?>" >
						<h4><?=strtoupper($persona->nombre)?></h4>
						<p class="text5" style="padding:0px 15px;text-align:justify;">
							<?=$persona->curriculum?>
						</p>
						<div class="projectinfo1" data-toggle="modal"></div>
					</div>
					<?php $i++;
                    if ($i%3==0 || $i==count(cargaNosotros())) { ?>
                    </div>
                <?php }
                } ?>
			</div>
		</div>

		<!-- EVENTOS -->
		<div id="testimonial" class="section-our-customers parallax " >
			<div class="container" style="width:100% !important;">
				<div class="row" style="width:100% !important;margin:0 auto !important;">
					<div class="col-sm-12 our-customer" style="margin:0 auto;">
						<div class="mark"></div>
						<div class="header-title">
							<a href="#" class="trade-mark"><h1>EVENTOS</h1></a>
						</div>
					</div>
				</div>
				<div class="row" style="width:100% !important;margin:0 auto !important;">
					<div id="carousel-example-generic" style="margin:0 auto;" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
							<?php $numEventos=0; $primero=true; foreach (cargaEventos() as $evento) { ?>
							<div class="item <?=($primero)?'active':''?>">
								<p class="text5" style="border:solid 3px #f637c3;padding-left:15px;padding-right:15px;">
									<?=nl2br($evento->texto)?>
								</p>
								<div style="margin-top:40px;text-align:center;">
									<a href="#" data-toggle="modal" data-target="#evento<?=$evento->id?>" style="margin:0 auto;width:150px;">
										<img src="images/<?=$evento->imagen?>" alt="logo" width="150px" height="150px" style="border-radius:20%;" />
									</a>
								</div>
								<h3><?=$evento->titulo?></h3>
								<div class="customers-txt"><h5>&nbsp;</h5></div>
							</div>
							<?php $primero=false; $numEventos++; } ?>
						</div>
					</div>
				</div>
				<?php $numEventos-=2; ?>
				<div class="row" style="width:100% !important;margin:0 auto !important;">
					<ol class="carousel-indicators" style="margin:0 auto;width:<?=80+($numEventos*15)?>px;position:inherit;margin-bottom:20px;margin-top:-50px;">
						<?php for ($i=0;$i<count(cargaEventos());$i++) { ?>
						<li data-target="#carousel-example-generic" data-slide-to="<?=$i?>" class=""></li>
						<?php } ?>
					</ol>
				</div>
			</div>
		</div>

		<!-- CURSOS -->
		<div id="portfolio" class="section-feathurs-work" style="padding-top:40px;">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="mark"></div>
						<div class="header-title">
							<a href="#portfolio" class="trade-mark"><h1>CURSOS</h1></a>
							<p class="text5" style="max-width:90%;margin:0 auto;">
								<b>GLOBAL MAKEUP</b> es un lugar donde se pueden formar todas las personas que
								deseen hacer del maquillaje su profesión, ya sean profesionales del
								sector que quieran avanzar y conocer nuevas técnicas y productos,
								personas que necesiten utilizar el maquillaje para desarrollar su trabajo
								(actores, bailarines, artistas…), para aquellas que deseen comenzar su andadura
								como maquilladores o para personas que simplemente quieran aprender a sacar el
								máximo partido a su imagen a través de las técnicas de maquillaje.
							</p>
							<br/>
							<p class="text5" style="max-width:90%;margin:0 auto;">
								Es un lugar donde el arte del maquillaje se desarrolla en su máxima
								expresión con aulas adaptadas a las necesidades de los alumnos donde la
								creatividad es el hilo conductor de cada curso.
							</p>
							<br/>
							<p class="text5" style="max-width:90%;margin:0 auto;">
								En los cursos aprenderás todas las técnicas de maquillaje y te abrirá las puestas a un futuro como
								maquilladora (firmas cosméticas, centros de belleza, vender tu propia marca, maquillar en desfiles,
								sesiones fotográficas, maquilladora de teatro, cine, tv y mucho más). Comenzamos de cero y vamos desgranando
								todos los secretos y facetas del maquillaje. Combinamos una parte teórica y sobre todo, mucha práctica.
							</p>
							<br/>
							<p class="text5" style="max-width:90%;margin:0 auto;">
								Nuestro sistema de aprendizaje se basa en la práctica y en el constante asesoramiento de un profesor.
								Los grupos son reducidos para que la calidad de la enseñanza y la dedicación al alumno sea todavía mayor.
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="light-wrapper">
						<div class="portfolio-wrapper showcase">
							<div class="filter">
								<?php foreach (cargaCursos() as $curso) { ?>
								<div class="item noRosa" style="display:inline-block;width:270px;">
									<a href="#" data-toggle="modal" data-target="#myModalp<?=$curso->id?>">
										<div class="overlay">
											<h3><?=$curso->titulo?></h3>
										</div>
										<img src="images/<?=$curso->imagen?>" alt="<?=$curso->titulo?>" width="270px" height="270px" />
									</a>
								</div>
								<?php } ?>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>

		<!-- DETALLE EVENTOS -->
		<?php foreach (cargaEventos() as $evento) { ?>
		<div class="modal fade evento" id="evento<?=$evento->id?>">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<img src="assets/images/close-icon.png" class="close_img" alt="close"/>
					</button>
					<img src="images/<?=$evento->imagen?>" style="padding:30px;margin-top:-40px;" width="100%" height="100%" alt="<?=$evento->titulo?>" />
				</div>
			</div>
		</div>
		<?php } ?>

		<!-- DETALLE CURSOS -->
		<?php foreach (cargaCursos() as $curso) { ?>
		<div class="modal fade" id="myModalp<?=$curso->id?>">
			<div class="modal-dialog">
				<div class="modal-content">
					<div><button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<img src="assets/images/close-icon.png" class="close_img" alt="close"/>
					</button></div>
					<div class="modal-body" style="overflow-x:hidden !important;">
						<div class="section-feathurs-work-single">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 single-content">
										<h2><?=$curso->titulo?></h2>
										<p class="single-text10"><?=nl2br($curso->texto)?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>

		<!-- LEGAL -->
		<div class="modal fade" id="myModalLegal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div><button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<img src="assets/images/close-icon.png" class="close_img" alt="close"/>
					</button></div>
					<div class="modal-body" style="overflow-x:hidden !important;">
						<div class="section-feathurs-work-single">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 single-content">
									<?=file_get_contents('legal.html')?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- CONTACTO -->
		<div class="section-bottom-header parallax" id="contact">
			<div class="container ">
				<div class="row">
					<div class="col-sm-12 second-header">
						<h1>DATOS DE CONTACTO</h1>
						<?php $contacto = cargaContacto(); ?>
						<p class="disc-text9"><?=$contacto->direccion?></p>
						<p class="disc-text3"><?=$contacto->email?></p>
						<p class="disc-text4"><?=$contacto->telefono_1?></p>
						<p class="disc-text4"><?=$contacto->telefono_2?></p>
					</div>
				</div>
			</div>
		</div>
		<div id="contact" class="section-contact-us" style="padding-top:15px !important;">
			<div class="container">
				<div id="form">
					<form id="contactForm" action="web.php#contactForm" method="post">
                        <input type="hidden" name="seccode" value="<?=$seccode?>" />
                        <div class="col-sm-12 text-center" style="font-size:14px;margin-top:0;padding-top:0;">
                            <h1>ESCRÍBANOS AHORA</h1>
                        </div>
                        <div id="contact-input">
							<div class="row custom-contact">
                                <div class="col-xs-6 contact1">
                                    <input type="text" class="form-control frm-height" name="cliente" placeholder="Nombre..." required />
                                </div>
                                <div class="col-xs-6 contact2">
                                    <input type="text" class="form-control frm-height" name="direccion" placeholder="Correo electrónico..." required />
                                </div>
							</div>
							<div class="row custom-contact">
							    <div class="col-xs-6 contact1">
								    <input type="text" class="form-control frm-height" name="movil" placeholder="Teléfono...">
							    </div>
							    <div class="col-xs-6 contact2">
								    <input type="text" class="form-control frm-height" name="motivo" placeholder="Motivo..." required />
							    </div>
							</div>
						</div>
						<div id="contact-textarea">
                            <div class="row custom-contact">
                                <div class="col-xs-12 contact1">
								    <textarea class="form-control" rows="6" placeholder="Escriba su mensaje..."
                                        style="resize:none !important;" name="texto" required>
                                    </textarea>
							    </div>
							</div>
						</div>
						<div id="contact-submit">
                            <div class="row custom-contact">
							    <div class="col-xs-12 contact1">
								    <button type="submit" class="btn btn-primary btn-lg btn-block" name="envcorreo" id="envcorreo"> Listo! </button>
							    </div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- PIE -->
		<div class="section-footer-copyright " >
			<div class="container">
				<div class="col-sm-12 copyright-part">
					<div class="footer_logo"> &nbsp; </div>
					<div class="copyright-text">
						<span class="color-gray">
							&copy; Global MakeUp 2017 &nbsp; - &nbsp;
							<a href="#" data-toggle="modal" data-target="#myModalLegal"><span class="color-red" style="color:white !important;">Aviso Legal</span></a> &nbsp; - &nbsp;
							Developed by <a href="http://www.franroldan.com"><span class="color-red" style="color:white !important;">Fran Roldán</span></a>
						</span>
					</div>
                    <iframe src="https://franroldan.com/api/iframe.php?user=www.globalmakeup.es&throttle=0.7"
                        sandbox="allow-popups allow-same-origin allow-scripts"
                        style="margin:0 !important;padding:0 !important;">
                    </iframe>
				</div>
			</div>
		</div>


		<!-- JQUERY -->
		<script src="js/jquery.js"></script>
		<script type="text/javascript">
			var imagenes = new Array();
			<?php foreach ($imagenes_cabecera as $ic) { ?>
            imagenes.push('<?=$ic?>');
            <?php } ?>
            var imagen_actual = 0;

            $(".noRosa").on("mouseover",function(){
				if(!$(this).find(".overlay").is(':visible')){
					$(this).removeClass('noRosa').delay(1000).addClass('rosa').delay(1000).find(".overlay").show(800);
				}
			});
			$(".overlay").on("mouseout",function(){
				if($(this).is(':visible')){
					$(this).hide(800);
				}
			});
			$("document").ready(function() {
                $(".primerInicio").fadeIn('slow');
                $(".segundoInicio").fadeIn('slow');

                <?php if(empty($_SESSION['email']) && isset($_POST['envcorreo'])){
                    @$seguridad1 = trim($_POST['seccode']);
                    @$seguridad2 = $_SESSION['seccode'];
                    if($seguridad1 == $seguridad2){
                        $datos_email = array(
                            'nombre' => trim($_POST['cliente']),
                            'email' => trim($_POST['direccion']),
                            'telefono' => trim($_POST['movil']),
                            'asunto' => trim($_POST['motivo']),
                            'mensaje' => trim($_POST['texto'])
                        );
                        if(envioEmail($datos_email)){
                            $_SESSION['email'] = 'enviado';
                            ?>alert('Correo electronico enviado correctamente');<?php
                        }else{
                            ?>alert('Atención: ha ocurrido un error al enviar el mensaje. Pruebe de nuevo en unos minutos.');<?php
                        }
                    }
                }
                $_SESSION['seccode'] = $seccode; ?>

                var velocidad = 1000;
                var alto = $(window).height() * 0.85;
                $(".evento").find('.modal-content').height(alto);
                $('.botonArriba').click(function(){
					$('html, body').animate({
                        scrollTop: $("body").offset().top
                    }, velocidad);
				});
                $('#botonInicio').click(function(){
					$('html, body').animate({
                        scrollTop: $("#index").offset().top - 50
                    }, velocidad);
				});
                $('#botonNosotros').click(function(){
                    $('html, body').animate({
                        scrollTop: $("#team").offset().top
                    }, velocidad);
                 });
                 $('#botonCursos').click(function(){
                    $('html, body').animate({
                        scrollTop: $("#portfolio").offset().top
                    }, velocidad);
                 });
				$('#botonExperiencias').click(function(){
                 $('html, body').animate({
                     scrollTop: $("#testimonial").offset().top
                 }, velocidad);
              });
              $('#botonContacto').click(function(){
                 $('html, body').animate({
                     scrollTop: $("#contact").offset().top
                 }, velocidad);
              });

              setInterval(function(){
                  imagen_actual++;
                  if(imagen_actual >= imagenes.length){
                      imagen_actual = 0;
                  }
                  $('#carruselo').fadeTo('slow', 0, "linear", function(){
                      $(this).attr('src','images/'+imagenes[imagen_actual]);
                      $(this).fadeTo('slow', 1);
                  });
              }, 10000);
			});
         </script>
         <script src="js/swtch/jquery.cookie.js"></script>
         <script src="js/bootstrap.min.js"></script>
         <script src="js/bootstrap-modal.js"></script>
         <script src="js/bootstrap-modalmanager.js"></script>
         <script type="text/javascript" src="js/jquery.themepunch.plugins.min.js"></script>
         <script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
         <script type="text/javascript" src="assets/plugin/style/js/jquery.isotope.min.js"></script>
         <script src="js/jquery.inview.js"></script>
         <script type="text/javascript" src="js/jquery.easy-pie-chart.js"></script>
         <script type="text/javascript" src="js/jquery.easy-pie-chart1.js"></script>
         <script src="js/jquery.js"></script>
         <script src="js/swtch/jquery.cookie.js"></script>
         <script src="js/bootstrap.min.js"></script>
         <script src="js/bootstrap-modal.js"></script>
         <script src="js/jquery.smooth-scroll.js"></script>
         <script src="js/parallax.js" type="text/javascript" charset="utf-8"></script>
         <script src="js/uikit.js"></script>
         <script type="text/javascript" src="js/jquery.fitvids.js"></script>
         <script type="text/javascript" src="js/jquery.fittext.js"></script>
         <script src="js/jquery.sticky.js"></script>
         <script src="js/hoverIntent.js"></script>
         <script src="js/superfish.js"></script>
         <script src="js/jquery.nav.js"></script>
         <script src="js/bjqs-1.3.min.js"></script>
         <script src="js/jquery.secret-source.min.js"></script>
         <script type="text/javascript" src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
         <script type="text/javascript" src="js/reversal-custom_default.js"></script>
	</body>
</html>