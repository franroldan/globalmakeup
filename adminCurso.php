<?php 
	session_start();
	require_once("util.php");
	$curso = cargaCurso((int)$_GET['curso']);
	
	// Loguin de administrador
	if(!isset($_SESSION['admin']) || is_null($curso)){
		header('Location:web.php');
	}
	
	// Editar curso
	if(isset($_POST['editarCurso']) && $_SESSION['admin']){
		if(editarCurso($curso->id,$_POST['texto'])){
			?><script type="text/javascript">
				alert('Curso editado correctamente');
				window.location = 'admin.php';
			</script><?php
		}else{
			?><script type="text/javascript">alert('Error: ha habido problemas para editar el curso');</script><?php
		}
	}
?>       
<!DOCTYPE html>
<html lang="es" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
	<head>
		<title>Global MakeUP</title>	
		<meta name="author" content="Fran Roldan" />
		<meta name="robots" content="index, follow" />
		<meta name="language" content="es" />
		<meta name="keywords" content="zaragoza,maquillaje,escuela,cursos,global makeup,bodas,modulos,profesion" />
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Haz de tu pasión tu profesión">
		<link rel="shortcut icon" href="assets/images/favicon.png" class="favicon_icon">
		<link rel="stylesheet" href="css/superfish.css" media="screen">	
		<link href="css/style_font.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/rs-plugin/css/responsive.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/rs-plugin/css/settings.css" media="screen" />
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
		<link href="css/style_default.css" rel="stylesheet" >	
		<link href="css/color_pink.css" rel="stylesheet" id="clrswitch">	
		<link href="css/color_bg_white.css" rel="stylesheet" id="clrswitch_bg">
		<link href="css/style-switcher.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="assets/plugin/style/css/style_iso.css" media="all" />
		<link rel="stylesheet" type="text/css" href="assets/plugin/style/css/media-queries.css" media="all" />
		<link href="css/uikit.css" rel="stylesheet">	
		<link href="css/bjqs.css" rel="stylesheet">	
		<link href="css/demo.css" rel="stylesheet">	
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->    
	</head>
	<body>  
		<nav class="light">
			<div class="navbar navbar-default navbar-static-top">
				<div class="container clearfix">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="http://www.globalmakeup.es" class="botonArriba">
							<p class="logo_main">
								<img src="assets/images/slide/logo.png" style="max-width:100%;margin-top:-10px;"/>
							</p>
						</a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="sf-menu" id="nav" style="margin-left:11.5%;"></ul>				
					</div>				
				</div>
			</div>
		</nav>
		<div class="section-more-feathurs" style="padding:0 !important;clear:both;" id="index">
			<div class="container feathurs-cointain">
				<div class="row">
					<h3 style="text-align:center;color:#f637c3;">Edición del curso</h3>
					<p style="padding:0px;margin:15px;text-align:center;">
						<?=$curso->titulo?>
						<hr style="padding:0;margin:0;">
					</p>
					<div class="col-sm-12 feathurs-part">				
						<p class="disc-text" style="margin-bottom:15px;">
							Para añadir textos en negrita, cursiva u otro 
							tipo de decoración debe investigar
							sobre etiquetas en formato HTML.
						</p>
						<form action="adminCurso.php?curso=<?=$curso->id?>" method="POST">
							<p class="disc-text">
								<textarea name="texto" style="width:80%;" rows="12" required><?=$curso->texto?></textarea>
								<br/><br/>
								<input type="submit" name="editarCurso" value="Editar curso" />
							</p>
						</form>
						<br/><br/>
					</div>
				</div>
			</div> 
		</div>	
		<div class="section-footer-copyright " >
			<div class="container">
				<div class="col-sm-12 copyright-part">
					<div class="footer_logo">
						<a href="#" class="botonArriba"><img src="assets/images/slide/logo.png" style="width:170px;"/></a>
					</div>
					<div class="clear"></div>
					<div class="copyright-text">
						<span class="color-gray">&copy; Global MakeUp 2015 - Developed by </span> 
						<a href="http://www.franroldan.com"><span class="color-red">Fran Roldán</span></a>
					</div>
				</div>			
			</div> 
		</div>
		<script src="js/jquery.js"></script>	
		<script type="text/javascript">
			$(".section-more-feathurs").hide();
			$("document").ready(function() {
                $(".section-more-feathurs").show(3000).delay(800);
				jQuery(document).ready(function($) {
					$('#banner-fade').bjqs({
						'responsive':true,
						'animtype':'slide'
					});
				});
			});
		</script>	
		<script src="js/swtch/jquery.cookie.js"></script> 		
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-modal.js"></script>
		<script src="js/bootstrap-modalmanager.js"></script>
		<script type="text/javascript" src="js/jquery.themepunch.plugins.min.js"></script>
		<script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="assets/plugin/style/js/jquery.isotope.min.js"></script>			
		<script src="js/jquery.inview.js"></script>		
		<script type="text/javascript" src="js/jquery.easy-pie-chart.js"></script>
		<script type="text/javascript" src="js/jquery.easy-pie-chart1.js"></script>	
		<script src="js/jquery.js"></script>		
		<script src="js/swtch/jquery.cookie.js"></script> 		
		<script src="js/bootstrap.min.js"></script>
		<script src="js/bootstrap-modal.js"></script>
		<script src="js/jquery.smooth-scroll.js"></script>	
		<script src="js/parallax.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/uikit.js"></script>	
		<script type="text/javascript" src="js/jquery.fitvids.js"></script>  
		<script type="text/javascript" src="js/jquery.fittext.js"></script> 
		<script src="js/jquery.sticky.js"></script>
		<script src="js/hoverIntent.js"></script>
		<script src="js/superfish.js"></script>		
		<script src="js/jquery.nav.js"></script>
		<script src="js/bjqs-1.3.min.js"></script>
		<script src="js/jquery.secret-source.min.js"></script>
		<script type="text/javascript" src="js/reversal-custom_default.js"></script>	
	</body>
</html>
		             
